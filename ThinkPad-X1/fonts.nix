{ config, pkgs, ... }:

{
  fonts = {
    enableDefaultFonts = true;
    fontDir.enable = true;
    fonts = [
      pkgs.fira
      pkgs.fira-code
      pkgs.hack-font
      pkgs.liberation_ttf
      pkgs.powerline-fonts
    ];
  };
}
