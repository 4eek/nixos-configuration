# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./fonts.nix
      ./zsh.nix
      ./ipfs.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # Add ZFS support.
  boot.supportedFilesystems = ["zfs"];
  boot.zfs.requestEncryptionCredentials = true;
  boot.loader.grub.copyKernels = true;

  networking.hostId = "9d7709ea";
  networking.hostName = "dukuduku"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s20f0u2u4.useDHCP = true;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  services.ntp.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Set your time zone.
  time.timeZone = null;
  # time.timeZone = "Africa/Johannesburg";

  # Nixos nixpkgs config.
  nixpkgs.config = {
    # Allow unfree so we can get firefox-dev, etc.
    allowUnfree = true;
    # allowBroken = true;
  };

  # Enable periodic automatic GC.
  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    alacritty
    any-nix-shell
    aria2
    ark
    aspell
    aspellDicts.en
    any-nix-shell
    bind
    bitwarden
    brave
    broot
    browsh
    cacert
    calcurse
    calibre
    coreutils
    cassowary
    chromium
    dbeaver
    dia
    direnv
    electrum
    element-desktop
    encfs
    exa
    fasd
    ffmpeg
    firefox
    fzf
    gcc
    gimp-with-plugins
    gitAndTools.gitFull
    gitAndTools.git-extras
    gitAndTools.gita
    gitAndTools.lab
    gitAndTools.hub
    gitAndTools.tig
    gitAndTools.gitAnnex
    gitAndTools.diff-so-fancy
    gitAndTools.grv
    git-repo-updater
    glow
    glxinfo
    gnupg
    guetzli
    hey
    htop
    # START image_optim
    image_optim
    advancecomp
    gifsicle
    jhead
    jpegoptim
    libjpeg
    lsof
    optipng
    pngcrush
    pngout
    pngquant
    # svgo
    # END
    inkscape-with-extensions
    jnettop
    jq
    iftop
    inotify-tools
    # kdeApplications.kruler
    kdenlive
    kdiff3
    keybase-gui
    kgpg
    kubectl
    krita
    lf
    libreoffice
    libxml2
    man
    mc
    mkpasswd
    (
      import ./neovim.nix
    )
    # masterpdfeditor
    nodejs
    nodePackages.eslint
    nmap
    nnn
    nox
    okular
    openssl.out
    openssl.dev
    pcloud
    polkit
    postman
    powertop
    protonvpn-cli
    protonvpn-gui
    rage
    ranger
    ripgrep
    rustup
    rust-analyzer
    s3fs
    sc-im
    signal-desktop
    simplescreenrecorder
    slack
    sops
    spectacle
    speedtest-cli
    standardnotes
    tmux
    todoist
    todoist-electron
    tor-browser-bundle-bin
    torbrowser
    tree
    unzip
    # veracrypt
    vscode-fhs
    vym
    wacomtablet
    wget
    xclip
    xorg.xbacklight
    # yed
    yubikey-manager
    yubikey-personalization-gui
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    # enableSSHSupport = true;
    pinentryFlavor = "qt";
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;
  programs.ssh.startAgent = true;

  # ZFS services
  services.zfs.autoSnapshot.enable = true;
  services.zfs.autoScrub.enable = true;

  # Enable Tor service
  services.tor.enable = true;

  # To use lorri for development
  services.lorri.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable Yubikey Agent service
  # services.yubikey-agent.enable = true;
  # services.pcscd.enable = true;

  # Enable Keybase services
  services.keybase.enable = true;
  services.kbfs.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # The locate service for finding files in the nix-store quickly.
  services.locate.enable = true;

  # Avahi is used for finding other devices on the network.
  services.avahi.enable = true;
  services.avahi.nssmdns = true;

  # Enable upower service
  services.upower.enable = true;
  powerManagement.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # bluetooth
  hardware.bluetooth.enable = true;

  # Enable TLP for optimal power saving
  # services.tlp.enable = true;
  services.tlp = {
    enable = true;
    settings = {
      START_CHARGE_THRESH_BAT0 = 75;
      STOP_CHARGE_THRESH_BAT0 = 80;

      # CPU_SCALING_GOVERNOR_ON_AC = schedutil;
      # CPU_SCALING_GOVERNOR_ON_BAT = schedutil;

      # CPU_SCALING_MIN_FREQ_ON_AC = 800000;
      # CPU_SCALING_MAX_FREQ_ON_AC = 3500000;
      # CPU_SCALING_MIN_FREQ_ON_BAT = 800000;
      # CPU_SCALING_MAX_FREQ_ON_BAT = 2300000;

      # # Enable audio power saving for Intel HDA, AC97 devices (timeout in secs).
      # # A value of 0 disables, >=1 enables power saving (recommended: 1).
      # # Default: 0 (AC), 1 (BAT)
      # SOUND_POWER_SAVE_ON_AC = 0;
      # SOUND_POWER_SAVE_ON_BAT = 1;

      # # Runtime Power Management for PCI(e) bus devices: on=disable, auto=enable.
      # # Default: on (AC), auto (BAT)
      # RUNTIME_PM_ON_AC = on;
      # RUNTIME_PM_ON_BAT = auto;

      # # Battery feature drivers: 0=disable, 1=enable
      # # Default: 1 (all)
      # NATACPI_ENABLE = 1;
      # TPACPI_ENABLE = 1;
      # TPSMAPI_ENABLE = 1;
    };
  };

  # This will save you money and possibly your life!
  services.thermald.enable = true;

  # To perform firmware upgrades just activate the service.
  # Then use fwupdmgr to perform updates.
  services.fwupd.enable = true;

  # Flatpak enable
  services.flatpak.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "ctrl:nocaps"; # Caps Lock as Ctrl

  # Enable touchpad support.
  services.xserver.libinput.enable = true;
  services.xserver.libinput.touchpad.naturalScrolling = true;
  services.xserver.libinput.touchpad.tapping = true;
  services.xserver.libinput.touchpad.disableWhileTyping = true;
  services.xserver.libinput.touchpad.accelSpeed = "0.9";

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Enable Syncthing.
  services.syncthing = {
      enable = true;
      dataDir = "/home/kgf/Sync";
      configDir = "/home/kgf/.config/syncthing";
      user = "kgf";
      openDefaultPorts = true;
      guiAddress = "0.0.0.0:8384";
  };

  # Enable Postgesql service
  services.postgresql.enable = true;
  services.postgresql.package = pkgs.postgresql_12;
  services.postgresql.authentication = lib.mkForce ''
    # Generated file; do not edit!
    # TYPE  DATABASE        USER            ADDRESS                 METHOD
    local   all             all                                     trust
    host    all             all             127.0.0.1/32            trust
    host    all             all             ::1/128                 trust
  '';


  # Virtualisation
  # virtualisation.virtualbox.host.enable = true;
  # virtualisation.virtualbox.host.enableExtensionPack = true;
  # users.extraGroups.vboxusers.members = [ "kgf" ];

  virtualisation.docker.enable = true;

  # Enable Wacom Tablet Service
  services.xserver.wacom.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.mutableUsers = false;
  users.users.kgf = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" "audio" "disk" "networkmanager" "docker" "ipfs" ];
    hashedPassword = let hashedPassword = import ./.hashedPassword.nix; in hashedPassword; # Make with mkpasswd
    shell = pkgs.zsh;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?

}

